import React from 'react';
import Home from './components/pages/home/Home'
import Areas from './components/pages/articles/Areas'
import Perfil from './components/pages/profile/Profile'
import { NewPost } from './components/pages/newposts/NewPost'
import Courses from './components/pages/courses/CursoProfile'
import Posts from './components/pages/posts/Posts'
import './assets/css/bootstrap-grid.min.css'
import './assets/css/bootstrap-reboot.min.css'
import './assets/css/bootstrap.min.css'
import './assets/libs/icon.css'
import './assets/custom_css/shards-dashboards.1.1.0.css'
import './assets/custom_css/extras.1.1.0.min.css'

import {BrowserRouter, Switch, Route} from 'react-router-dom'
 

function App() {

  return (
    <BrowserRouter>
        <Switch>
          <Route path="/" exact={true} component={Home}/>
          <Route path="/areas" component={Areas}/>
          <Route path="/profile" component={Perfil}/>
          <Route path="/newpost" component={NewPost}/>
          <Route path="/newcourse" component={Courses}/>
          <Route path="/posts" component={Areas}/>
          <Route path="/artigo:curso" component={Posts}/>
        </Switch>
    </BrowserRouter> 
  );
}

export default App;
