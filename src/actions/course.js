import firebase from './../config/firebase'


export var createCourse = (data)=>{

    firebase.firestore().collection('courses').add(data)
    .then(function() {
        console.log("Document successfully written!");
    })
    .catch(function(error) {
        alert("Error writing document: ", error);
    });

}


export  async function listCourses (){
    var resultados = {
        data : [],
        id : [],
    };

    var coursesRef = firebase.firestore().collection('courses');
    var query = await coursesRef.where("campus", "==", "teste")
    .get().then((querySnapshot)=>{
        querySnapshot.forEach(function(doc){
            resultados.data.push(doc.data());
            resultados.id.push(doc.id);
        });    
    });

    return resultados;
}
