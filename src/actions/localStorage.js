
export function setStorage(data){
    localStorage.setItem("logado", data.logado);
    localStorage.setItem("route", data.route);
}

export function getStorage(){
    var data = {};
    data.logado = localStorage.getItem('logado');
    data.route = localStorage.getItem('route');
    return data; 
}

export function deleteStorage(){
    localStorage.clear();
}

