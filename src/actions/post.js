import firebase from './../config/firebase'


export var createPost = (data)=> {

    firebase.firestore().collection('posts').add(data)
    .then(function() {
        alert("Document successfully written!");
        return true;
    })
    .catch(function(error) {
        alert("Error writing document: ", error);
        return false;
    });

}

export  async function listPosts (ref) {
    
    var resultados = {
        data : [],
        id : [],
    };

    var postsRef = firebase.firestore().collection('posts');

    var query = await postsRef.where("idCurso", "==", ref)
    .get().then((querySnapshot) => {
       
        querySnapshot.forEach( function(doc) {

            resultados.data.push(doc.data());
            resultados.id.push(doc.id);
            
        });    
    });

    return resultados;
}
