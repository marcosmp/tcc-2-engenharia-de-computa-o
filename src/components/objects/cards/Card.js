import React, { Component } from 'react'
import img from './../../../sources/content-management/3.jpeg'
import avatar from './../../../sources/avatars/0.jpg'
import {Link} from 'react-router-dom'

export default class Card extends Component {

    constructor(props){
        super(props);
    } 

   
    render() {
        return (
            <div className="col-lg-3 col-md-6 col-sm-12 mb-12" style={{marginBottom: 20}} key={this.props.id}>
                {this.props.children}
                <div className="card card-small card-post card-post--1">
                    <div className="card-post__image" style={{ backgroundImage: `url("${img}")` }}>
                    <Link to='/posts'><div className="card-post__category badge badge-pill badge-dark">curso</div></Link>
                        <div className="card-post__author d-flex">
                            <div href="#" className="card-post__author-avatar card-post__author-avatar--small" style={{ backgroundImage: `url("${avatar}")` }}>Written by Anna Kunis</div>
                        </div>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">
                            <div className="text-fiord-blue" href="#">{this.props.title}</div>
                        </h5>
                        <span className="text-muted">{this.props.subtitle}</span> 
                        <Link to={this.props.link}><button className="btn btn-outline-success btn-sm float-right">{this.props.action}</button></Link>
                    </div>
                </div>
            </div>
        )
    }
}