import React from 'react'
import img from './../../../sources/avatars/unievangelica.png'

export const  Instituicao = (props) =>{
    return(
        <div className="col-lg-12">
            <div className="card card-small mb-4 pt-3" style={{borderTopLeftRadius:0, borderTopRightRadius:0}}>
                <div className="card-header border-bottom text-left">
                    <div className="mb-3 mx-auto">
                        <img className="rounded-circle" src={img} alt="User Avatar" style={{width:110, height:110}}/> 
                    </div>
                        <h4 className="mb-0">{props.nome}</h4>
                        <button type="button" className="mb-2 btn btn-sm btn-pill btn-outline-primary mr-2">
                            <a href="https://www.unievangelica.edu.br/novo/index.php" ><i className="material-icons mr-1">school</i>Instituição</a>
                        </button>
                </div>
            </div>
        </div>
    )
}