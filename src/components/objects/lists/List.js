import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import img from './../../../sources/content-management/17.png'

export default class List extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="col-lg-12 col-md-12 col-sm-12 mb-12" style={{marginBottom: 20}}>
                <div className="card card-small card-post card-post--aside card-post--1">
                    <div className="card-post__image" style={{ backgroundImage: `url("${img}")` }}>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">
                            <a className="text-fiord-blue" href={this.props.link}>{this.props.titulo}</a>
                        </h5>
                        <p className="card-text d-inline-block mb-3">
                            {this.props.resumo}
                        </p>
                         <span className="text-muted">{this.props.data}</span>
                    </div>
                </div>
            </div>
        )
    }
}