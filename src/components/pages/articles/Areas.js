import React from 'react';
import Header from './../template/Header'
//import Card from './../../objects/cards/Card'
import CoursesList from './../courses/CoursesList'
import {Instituicao} from './../../objects/instituicao/Instituicao'
import { NavbarLateral } from './../template/NavbarLateral'
import { getStorage } from './../../../actions/localStorage'


export default class Areas extends React.Component {

     valor = "Unievangélica - Centro Universitário"
     

    render() {

        var sessao = getStorage();

        return (
            <div className="col-md-12">
                <div className="row">
                    <Header/>
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <NavbarLateral />
                    </div>
                    <div className="col-md-10">
                        <div className="row">
                          <Instituicao nome={this.valor}/>
                        </div>
                        <div className="row">
                            <CoursesList/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



