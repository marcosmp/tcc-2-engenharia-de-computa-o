import React from 'react'
import Card from './../../objects/cards/Card'
import { listCourses } from './../../../actions/course'


export default class CoursesList extends React.Component{
    
    state = {
        cursos:[],
        ids:[]
    }

    async componentDidMount(){
        const response = await listCourses();
        this.setState({cursos: response.data})
        this.setState({ids: response.id})
    }

    render(){

        return(
            <div className="col-md-12">
                <div className="row">
                {
                    this.state.cursos.map((dados, index) =>(

                        <Card title={dados.nomeCurso} subtitle={dados.turno} action={"Inscrever"} link={'artigo'+this.state.ids[index]} />
                    ))
                } 
                </div>
            </div>
        
        )
    }

}
