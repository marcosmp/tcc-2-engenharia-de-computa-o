import React from 'react'
import Header from '../template/Header'
import { Avatar } from './Avatar'
import Form  from './Form'
import {NavbarLateral} from '../template/NavbarLateral'

export default class CursoProfile extends React.Component {
    render() {
        return (
            <div className="col-md-12">
                <div className="row">
                    <Header />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <NavbarLateral/>
                    </div>
                    <div className="col-md-10">
                        <div className="row">
                            <Avatar/>
                            <Form/>
                        </div>
                    </div>
                </div>
            </div>   
        );
    }
}