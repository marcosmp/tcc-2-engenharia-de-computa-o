import React from 'react'
import {createCourse, listCourses} from './../../../actions/course';


export default class Form  extends React.Component {

    constructor(props){
        super(props);

        this.state = {

            nomeCurso: "",
            instituicao: 0,
            grau:"",
            modalidade: "",
            campus: "",
            turno: "",
            descricao: "",
            image: null
        }
    } 

    handleImageChange = e =>{

        if (this.files && this.files[0]) {
      
            var FR= new FileReader();
            
            FR.addEventListener("load", function(e) {
              console.log(e.target.result);
            }); 
            
            FR.readAsDataURL( this.files[0] );
          }


        //this.setState({image: e.target.files[0]});
    }

    render(){
        return(
            <div className="col-lg-8">
                <div className="card card-small mb-4" style={{marginTop:25}}>
                    <div className="card-header border-bottom">
                        <h6 className="m-0">Detalhes do Perfil</h6>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item p-3">
                            <div className="row">
                                <div className="col">
                                    <div>
                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label for="feFirstName">Curso</label>
                                                <input type="text" className="form-control" id="nomeCurso" placeholder="Nome" 
                                                    onInput={(e) => this.setState({nomeCurso: e.target.value})}
                                                /> 
                                            </div>
                                            <div className="form-group col-md-6">
                                            <label for="feInputState">Insituição</label>
                                                <select id="instituicao" className="form-control" 
                                                    onChange={(e) => this.setState({instituicao: e.target.value})
                                                    }>
                                                    <option selected>Choose...</option>
                                                    <option>Unievangélica</option>
                                                    <option>Anhanguera</option>
                                                    <option>UEG</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label for="feEmailAddress">Grau</label>
                                                <input type="email" className="form-control" id="grau" placeholder="Grau"
                                                     onInput={(e) => this.setState({grau: e.target.value})}
                                                /> 
                                            </div>
                                            <div className="form-group col-md-6">
                                                <label for="fePassword">Modalidade</label>
                                                <input type="text" className="form-control" id="modalidade" placeholder="Modalidade"
                                                    onInput={(e) => this.setState({modalidade: e.target.value})}
                                                /> 
                                            </div>
                                        </div>
    
                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label for="feFirstName">Campus</label>
                                                <input type="text" className="form-control" id="campus" placeholder="Campus"
                                                    onInput={(e) => this.setState({campus: e.target.value})}
                                                /> 
                                            </div>
                                            <div className="form-group col-md-6">
                                            <label for="feInputState">Turno</label>
                                                <select id="turno" className="form-control"
                                                    onChange={(e) => this.setState({turno: e.target.value})
                                                }>
                                                    <option selected>Choose...</option>
                                                    <option>Matutino</option>
                                                    <option>Vespertino</option>
                                                    <option>Noturno</option>
                                                </select>
                                            </div>
                                        </div>
                                        
    
                                        <div className="form-row">
                                            <div className="form-group col-md-12">
                                                <label for="feDescription">Sobre o curso:</label>
                                                <textarea className="form-control" name="feDescription" rows="10" id="descricao"
                                                    onInput={(e) => this.setState({descricao: e.target.value})}
                                                ></textarea>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="form-group col-md-12">  
                                                    <input type="file" classNAme="custom-input" id="imageUpload"
                                                        onChange={this.handleImageChange}
                                                    />
                                            </div>
                                        </div>
    
                                        <button  className="btn btn-accent" onClick={()=>{insertCourse(this.state)}}>Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }

}

function insertCourse(val){
    createCourse(val);
}
