import React from 'react';
import  {createUser}    from './../../../actions/user';


export const FormInicio = ()=>{
    return(
        <div className="col-md-12">

            <div className="row">
                <div className="col-md-2"></div>
                <div className="col-md-8" style={{backgroundColor:'white', borderRadius:10, marginTop:25, paddingBottom:25, boxShadow: '0px 4px 14px -5px rgba(0,0,0,0.75)'}}>
                    <div class="card-header border-bottom" style={{backgroundColor:'white'}}>
                        <h6 class="m-0">Detalhes da Conta</h6>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="feFirstName">Nome</label>
                            <input type="text" className="form-control" id="nome" placeholder="Nome"/> 
                        </div>
                        <div className="form-group col-md-6">
                            <label for="feLastName">Sobrenome</label>
                            <input type="text" className="form-control" id="sobrenome" placeholder="Sobrenome" /> 
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="instituicao">Instituição</label>
                        <select id="instituicao" class="form-control">
                            <option selected>Choose...</option>
                            <option>UniEvangélica</option>
                        </select>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="curso">Curso</label>
                        <select id="curso" class="form-control">
                            <option selected>Choose...</option>
                            <option>Engenharia de Computação</option>
                        </select>
                        </div>
    
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" placeholder="Email"/> 
                        </div>
                        <div class="form-group col-md-6">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control" id="senha" placeholder="Senha"/> 
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                        <label for="dia">Dia</label>
                        <input type="number" class="form-control" id="dia"/> 
                        </div>
                        <div class="form-group col-md-6">
                        <label for="mes">Mês</label>
                        <select id="mes" class="form-control">
                            <option selected value="1">Janeiro</option>
                            <option value="2">Fevereiro</option>
                            <option value="3">Março</option>
                            <option value="4">Abril</option>
                            <option value="5">Maio</option>
                        </select>
                        </div>
                        <div class="form-group col-md-3">
                        <label for="ano">Ano</label>
                        <input type="number" class="form-control" id="ano"/> 
                        </div>
                    </div>
                    <div className="form-group col-md-12">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="sexo" value="masculino"/>
                            <label class="form-check-label" for="inlineRadio1">Masculino</label>
                        </div>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="sexo" value="feminino"/>
                            <label class="form-check-label" for="inlineRadio2">Feminino</label>
                        </div>
                    </div>
                    <div className="form-group col-md-12">
                         <button type="submit" class="btn btn-success" onClick={cadastrar}>Criar Conta</button>
                    </div>
                </div>
            </div>
        </div>
    )
}


const cadastrar = () =>{

    const user = {
        'nome': document.getElementById('nome').value,
        'sobrenome': document.getElementById('sobrenome').value,
        'dia': document.getElementById('dia').value,
        'mes': document.getElementById('mes').value,
        'ano': document.getElementById('mes').value,
        'email':'meuid3@gmail.com',
        'senha':'123456'
    }
    createUser(user.email, user.senha);

}