import React from 'react'
import {Inicio} from './Inicio'
import Header from './../template/Header'

export default class Home extends React.Component{

    render(){
        return(
            <div>
                <div>
                    <Header/>
                </div>
                <div>
                    <Inicio/>
                </div>
            </div>
        )
    }

}