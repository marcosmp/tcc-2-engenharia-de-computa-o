import React from 'react';
import {FormInicio} from './FormInicio'
import {TextIntroducao} from './TextIntroducao'


export  const Inicio = () =>{
    return(
        <div className="col-md-12">
            <div className="row"> 
                <div className="col-md-6">
                    <TextIntroducao/>
                </div>
                <div className="col-md-6">
                    <FormInicio/>
                </div>
            </div>
        </div>
    )
}   