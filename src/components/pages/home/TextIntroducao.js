import React from 'react';


export const TextIntroducao = ()=>{
    return(
        <div className="col-md-12" style={{marginTop:25}}>
            <div className="row">
                <div className="col-md-4"></div>
                <div className="col-md-8">
                    <div style={{textAlign:"justify"}}>
                        <p >Bem vindo ao </p>
                        <h1>Semear</h1>
                        <p>
                            O SEMEAR é uma plataforma de publicação e compartilhamento  de trabalhos
                            e artigos acadêmicos que disponibiliza um acervo de conteúdos aprovados
                            no intuito de icentivar discentes e docentes a desenvolverem conteúdos com 
                            maior qualidade.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}