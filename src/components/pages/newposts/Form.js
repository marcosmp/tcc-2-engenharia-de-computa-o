import React from 'react'
import { listCourses } from './../../../actions/course';
import { createPost } from './../../../actions/post';
import icon from './../../../sources/navicons/uploadArticle.svg';
import  firebase  from './../../../config/firebase';
import  {storage}  from './../../../config/firebase';

export class Form  extends React.Component{
    
    constructor(props) {
        super(props);
        this.novoPost = this.novoPost.bind(this);
        this.uploadArtigo = this.uploadArtigo.bind(this);
        
        this.state = {
            cursos: [],
            ids:[] 
        }
    }

    dados = {
        titulo: "",
        resumo: "",
        idCurso: "", 
        fileRef: "",
        url:"",
        dataPublicacao: ""
    }

    async componentDidMount() {

        const response = await listCourses();
        this.setState({cursos: response.data})
        this.setState({ids: response.id})
    }

    novoPost = (values) =>{
        if(values.titulo !== "" && values.resumo !== "" && values.idCurso !== ""){
            console.log(values.fileRef);
            if(values.fileRef !== "" && values.fileRef !== null){
                this.dados.dataPublicacao = new Date();
                createPost(values);
            }
            else{
                alert("Anexe o arquivo!!");
            }
            
        }
     }

    uploadArtigo = (e) =>{

        let files = e.target.files[0];
        let uploader = document.getElementById('uploader');
        let timestamp = new Date().getTime();

        const storageRef = storage.ref('artigos/'+files.name+'_'+timestamp);
        this.dados.fileRef = 'artigos/'+files.name+'_'+timestamp;
    
        let task = storageRef.put(files);
    
        task.on('state_changed', (snapshot)=>{
            let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            uploader.value = percentage;
        }, 
        (err)=>{
    
        },
        complete =>{
            alert("upload Concluido!!");
            task.snapshot.ref.getDownloadURL().then((url) => {
                this.dados.url = url;
            });
        });
    }    


    render() {

        return (
            <div className="col-md-12">
               
                <div className="col-lg-12 col-md-12">
                    <div className="card card-small mb-3">
                        <div className="card-body">
                        <form action="#">
                            <div className="add-new-post">
                                <div className="form-row">
                                    <div className="card-header" style={{backgroundColor: 'white'}}>
                                        <h6 className="m-0"><img src={icon} style={{width:35, height:35}}/> Novo Post</h6>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <input className="form-control" type="text" placeholder="Título do Trabalho"
                                            onInput={(e) => this.dados.titulo = e.target.value}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <select id="curso" className="form-control" onChange={(e) => this.dados.idCurso = e.target.value}>
                                            {
                                                this.state.cursos.map((dados, index) =>(
                                                    <option value={this.state.ids[index]} key={dados.nomeCurso}> {dados.nomeCurso} </option>
                                                ))
                                            } 
                                        </select>
                                    </div>
                                </div>

                                <div id="editor-container" className="add-new-post__editor mb-1">
                                     <textarea className="form-control" id="exampleFormControlTextarea1" rows="15" placeholder="Resumo do artigo"
                                         onInput={(e) => this.dados.resumo = e.target.value}
                                     >
                                     </textarea>
                                </div>
    
                                <div className="form-group">
                                    <label >Anexe o arquivo em formato PDF!</label>
                                    <input type="file" className="form-control-file" id="fileButton"
                                        onChange={(e)=>this.uploadArtigo(e)}
                                    />
                                </div>
                                <div>
                                    <progress value="0" max="100" id="uploader" style={{width: '100%'}}>0%</progress>
                                </div>
                                <button className="btn btn-success float-rigth" onClick={this.novoPost(this.dados)}>Salvar</button>
                            </div>
                        </form>   
                        </div>
                    </div>
                </div>
            </div>
        )        
    }

}


