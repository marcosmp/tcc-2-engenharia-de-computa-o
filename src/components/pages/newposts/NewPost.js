import React from 'react'
import Header from './../template/Header'
import { NavbarLateral } from './../template/NavbarLateral'
import { Form } from './Form'


export const NewPost = () =>{
    return(
        <div className="col-md-12">
            <div className="row">
                <Header/>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <NavbarLateral/>
                </div>
                <div className="col-md-10">
                    <div className="row">
                        <Form/>
                    </div>
                </div>
            </div>
        </div>
    )
}