import React from 'react';
import Header from './../template/Header'
import { NavbarLateral } from './../template/NavbarLateral'
import { getStorage } from './../../../actions/localStorage'
import { Instituicao } from './../../objects/instituicao/Instituicao'
import  PostsList  from './PostsList'


export default class Posts extends React.Component {

    parametro = '';

    constructor(props) {

        super(props);
        this.parametro = this.props.match.params.curso
    }

    valor = "Engenharia de computação";

    render() {

        var sessao = getStorage();

        return (
            <div className="col-md-12">
                <div className="row">
                    <Header />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <NavbarLateral />
                    </div>
                    <div className="col-md-10">
                        <div className="row">
                            <Instituicao nome={this.valor} />
                        </div>
                        <div className="row">
                             <PostsList link={this.parametro}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}