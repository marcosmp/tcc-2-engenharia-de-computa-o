import React from 'react'
import List from './../../objects/lists/List'
import { listPosts } from './../../../actions/post'


export default class PostsList extends React.Component{
    
    state = {
        posts:[],
        ids:[]
    }

    constructor(props) {

        super(props);
    
    }

    async componentDidMount(){

        const response = await listPosts(this.props.link);
        this.setState({posts: response.data})
        this.setState({ids: response.id})

    }

    render(){

        return(
            <div className="col-md-12">
                
                <div className="row" key={this.state.posts.ids}>
                {
                    this.state.posts.map((dados, index) =>(
                        <List titulo={dados.titulo} resumo={dados.resumo} arquivo={dados.fileRef} link={dados.url} /> 
                    ))
                } 
                </div>
            </div>
        
        )
    }

}
