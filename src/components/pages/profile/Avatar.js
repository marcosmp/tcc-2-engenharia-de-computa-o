import React from 'react'
import img from './../../../sources/avatars/user.png'

export const Avatar = () =>{
    return(
        <div className="col-lg-4">
            <div className="card card-small mb-4 pt-3" style={{marginTop:25}}>
                <div className="card-header border-bottom text-center">
                    <div className="mb-3 mx-auto">
                        <img className="rounded-circle" src={img} alt="User Avatar" style={{width:110, height:110}}/> 
                    </div>
                        <h4 className="mb-0">Nome</h4>
                        <span className="text-muted d-block mb-2">Nome perfil</span>
                        <button type="button" className="mb-2 btn btn-sm btn-pill btn-outline-primary mr-2">
                            <i className="material-icons mr-1">person_add</i>Perfil
                        </button>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item p-4">
                        <strong className="text-muted d-block mb-2">Descrição</strong>
                        <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</span>
                    </li>
                </ul>
            </div>
        </div>
    );
}