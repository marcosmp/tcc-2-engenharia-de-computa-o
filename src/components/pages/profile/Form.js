import React from 'react'


export const Form = () =>{
    return(
        <div className="col-lg-8">
            <div className="card card-small mb-4" style={{marginTop:25}}>
                <div className="card-header border-bottom">
                    <h6 className="m-0">Detalhes do Perfil</h6>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item p-3">
                        <div className="row">
                            <div className="col">
                                <div>
                                    <div className="form-row">
                                        <div className="form-group col-md-6">
                                            <label for="feFirstName">Nome</label>
                                            <input type="text" className="form-control" id="nome" placeholder="Nome"/> 
                                        </div>
                                        <div className="form-group col-md-6">
                                            <label for="feLastName">Sobrenome</label>
                                            <input type="text" className="form-control" id="sobrenome" placeholder="Sobrenome"/> 
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-group col-md-6">
                                            <label for="feEmailAddress">Email</label>
                                            <input type="email" className="form-control" id="email" placeholder="Email"/> 
                                        </div>
                                        <div className="form-group col-md-6">
                                            <label for="fePassword">Senha</label>
                                            <input type="password" className="form-control" id="senha" placeholder="Senha"/> 
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label for="feInputAddress">Endereço</label>
                                        <input type="text" className="form-control" id="endereco" placeholder="Endereço"/> 
                                    </div>

                                    <div className="form-row">
                                        <div className="form-group col-md-6">
                                            <label for="feInputCity">Cidade</label>
                                            <input type="text" className="form-control" id="cidade"/> 
                                        </div>
                                        <div className="form-group col-md-4">
                                            <label for="feInputState">Estado</label>
                                            <select id="estado" className="form-control">
                                                <option selected>Choose...</option>
                                                <option>MG</option>
                                                <option>GO</option>
                                                <option>SP</option>
                                            </select>
                                        </div>
                                        <div className="form-group col-md-2">
                                            <label for="inputZip">CEP</label>
                                            <input type="text" className="form-control" id="cep"/>
                                        </div>
                                    </div>

                                    <div className="form-row">
                                        <div className="form-group col-md-12">
                                            <label for="feDescription">Descrição</label>
                                            <textarea className="form-control" name="feDescription" rows="5" id="descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</textarea>
                                        </div>
                                    </div>

                                    <button type="submit" className="btn btn-accent">Atualizar Dados</button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    )
}