import React from 'react';
import {Link, Redirect}  from 'react-router-dom';
import firebase from './../../../config/firebase';
import {setStorage, getStorage, deleteStorage} from './../../../actions/localStorage';
import exit from './../../../sources/navicons/exit.svg';
import LogoSemear from './../../../sources/navicons/LogoSemear.svg';


export  default class Header extends React.Component{

    auth = {
        user:"",
        password:""
    }

    constructor(props){
        super(props);

        this.state = {
            logado: false,
            route: "/"
        }
    }


    logIn = () => {

        if(this.auth.user !== '' && this.auth.password !== ''){
            firebase.auth().signInWithEmailAndPassword(this.auth.user, this.auth.password)
            .then((response) =>{
                
                this.setState({logado: true, route: "/areas"});
                setStorage(this.state);

            }).catch(function(error) {
                alert(error);
            });
        }
    }

    signOut = () =>{
        firebase.auth().signOut().then(function() {
           deleteStorage()
          }, 
          function(error) {
            console.error( error );
          });
    }
    
    renderRedirect = () => {
        if (this.state.logado === true) {

          setTimeout(function(){ window.location.reload() }, 50);
          return <Redirect to='/areas' />
         
        }
        
    }

    render(){
        return(
            
            <div className="col-md-12" style={{minHeight: 70,paddingTop:20, backgroundColor:'#057550', marginBottom: 5}}>
                {this.renderRedirect()}
                
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-4" style={{color:'white'}}><img src={LogoSemear} style={{width:50, height:50 }}/> SEMEAR</div>
                    <div className="col-md-5">
                      
                        <div className="form-row">
                            <div className="form-group col-md-4">
                                {!getStorage().logado && <input className="form-control form-control-sm" id="usuario" type="text" placeholder="Usuário"
                                    onInput={(e) => this.auth.user = e.target.value}
                                />}
                            </div>
                            <div className="form-group col-md-4">
                                {!getStorage().logado && <input className="form-control form-control-sm" type="password" placeholder="Senha"
                                    onInput={(e) => this.auth.password = e.target.value}
                                />}
                            </div>
                            <div className="form-group col-md-2 col-sm-6 col-sx-12">
                               {!getStorage().logado && <Link to={this.state.route}><button className="btn btn-primary btn-sm" onClick={this.logIn()}>Entrar</button></Link>}
                            </div>
                            <div className="form-group col-md-2 col-sm-6 col-sx-12">
                               {getStorage().logado && <Link to='/'><button className="btn btn-outline-success btn-sm" onClick={deleteStorage}> <img src={exit}/></button></Link>}
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
        )
    }
   
}





