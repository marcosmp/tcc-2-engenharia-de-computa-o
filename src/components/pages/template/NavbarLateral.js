import React from 'react'
import {Link} from 'react-router-dom'
import './navbarLateral.css'
import cursos from './../../../sources/navicons/cursos.svg'
import home from './../../../sources/navicons/home.svg'
import book from './../../../sources/navicons/book.svg'
import moderar from './../../../sources/navicons/moderar.svg'
import newpost from './../../../sources/navicons/newpost.svg'




export const NavbarLateral = () => {
    return (
           <div className="row">
               <div className="col-md-12" style={{padding: 0}}>
                   <ul className="link">
                       <Link to='/' className="route"><li><img src={home}/> Início</li></Link>
                       <Link to='/areas' className="route"><li><img src={cursos}/>Cursos</li></Link>
                       <li><img src={book}/>Publicações</li>
                       <li><img src={moderar}/>Moderar Publicações</li>
                       <Link to='/profile' className="route"><li><img src={moderar}/>Perfil </li></Link>
                       <Link to='/newpost' className="route"><li><img src={newpost}/>Novo Post </li></Link>
                       <Link to='/newcourse' className="route"><li><img src={moderar}/>Cadastrar Curso </li></Link>
                   </ul>
               </div>
           </div>
    )
}
