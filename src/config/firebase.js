import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const firebaseConfig = {
    /*apiKey: "AIzaSyCYJAoVQRbDuVUCEa-IRRpUkX2bvJJ_JtI",
    authDomain: "semear-202fc.firebaseapp.com",
    databaseURL: "https://semear-202fc.firebaseio.com",
    projectId: "semear-202fc",
    storageBucket: "",
    messagingSenderId: "52381122113",
    appId: "1:52381122113:web:b7aef8f51a84b04b"*/


    apiKey: "AIzaSyAGju83fkzl73M4LufbKVH1cogT1WcbdA8",
    authDomain: "semear-2.firebaseapp.com",
    databaseURL: "https://semear-2.firebaseio.com",
    projectId: "semear-2",
    storageBucket: "semear-2.appspot.com",
    messagingSenderId: "947429628281",
    appId: "1:947429628281:web:f7fe9b97491d72839037a8",
    measurementId: "G-878QDDBCSW"
  };


  firebase.initializeApp(firebaseConfig);

  const storage = firebase.storage();

  export {storage, firebase as default} ;



